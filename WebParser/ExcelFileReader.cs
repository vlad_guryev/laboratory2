﻿using ExcelDataReader;
using System.Data;
using System.IO;

namespace WebParser
{
    class ExcelFileReader
    {
        //should be try-catched in embraced code
        public static DataSet GetExcelDataSet(string filePath)
        {
            DataSet result;
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    result = reader.AsDataSet();
                }
            }
            return result;
        }
    }
}
