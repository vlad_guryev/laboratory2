﻿using System;
using System.Collections.Generic;
using System.Data;

namespace WebParser
{
    class TableManager
    {
        public static List<DataTable> Paginate(DataTable sourceTable, int pageSize = 20)
        {
            int num = 0;
            var overallRows = sourceTable.Rows.Count;
            var pagesCount = overallRows / pageSize + 1;
            List<DataTable> tables = new List<DataTable>(pagesCount);

            DataTable subTable;
            for (int i = 0; i < pagesCount; i++)
            {
                subTable = sourceTable.Clone();
                subTable.TableName = "Table_" + i;
                subTable.Clear();
                for (int j = 0; (j < pageSize) && (num < overallRows); j++)
                {
                    DataRow newRow = subTable.NewRow();
                    newRow.ItemArray = sourceTable.Rows[j + i * pageSize].ItemArray;
                    subTable.Rows.Add(newRow);
                    num++;
                }
                tables.Add(subTable);
            }
            return tables;
        }

        private static DataTable Transpose(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();
            // Add columns by looping rows
            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();
                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }
            return outputTable;
        }

        public static DataTable BuildEntryTable(DataTable DbTable, DataRow row)
        {
            DataTable Entry = DbTable.Clone();
            Entry.Clear();
            DataRow dbHeader = DbTable.Rows[1];

            DataRow newRow = Entry.NewRow();
            newRow.ItemArray = dbHeader.ItemArray;
            Entry.Rows.Add(newRow);

            newRow = Entry.NewRow();
            newRow.ItemArray = row.ItemArray;
            Entry.Rows.Add(newRow);

            return Transpose(Entry);
        }

        public static DataTable GetNewTableChanges(DataTable oldTable, DataTable newTable)
        {
                DataTable difference = new DataTable();
                difference = newTable.Clone();
                Tuple<DataTable, int> MinTable = oldTable.Rows.Count > newTable.Rows.Count ?
                    new Tuple<DataTable, int>(newTable, newTable.Rows.Count):
                    new Tuple<DataTable, int>(oldTable, oldTable.Rows.Count);

                Tuple<DataTable, int> MaxTable = oldTable.Rows.Count > newTable.Rows.Count ?
                    new Tuple<DataTable, int>(oldTable, oldTable.Rows.Count):
                    new Tuple<DataTable, int>(newTable, newTable.Rows.Count);

//Это просто адовый ад. Я не понимаю, почему в одной версии таблицы знаки 
//переноса строк воспринимаются как символы, а в другой нет. Данный костыль призван решить проблему сравнения
//одинаковых строк, различающихся только символами _x000d_ \r\n. Ужасно некрасиво, но по-другому не выходило=(((
//Однако убрать из самой таблицы знаки _x000d_ \r\n мне не удалось. 
            string[] MaxTableRows = new string[MaxTable.Item2];
            for(int i = 0; i < MaxTable.Item2; i++)
            {
                for (int k = 0; k < MaxTable.Item1.Rows[i].ItemArray.Length; k++)
                {
                    string replaced = MaxTable.Item1.Rows[i].ItemArray[k].ToString().Replace("_x000d_", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                    MaxTableRows[i] += replaced;
                }
            }

            string[] MinTableRows = new string[MinTable.Item2];
            for (int i = 0; i < MinTable.Item2; i++)
            {
                for (int k = 0; k < MinTable.Item1.Rows[i].ItemArray.Length; k++)
                {
                    string replaced = MinTable.Item1.Rows[i].ItemArray[k].ToString().Replace("_x000d_", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                    MinTableRows[i] += replaced;
                }
            }


            for (int i = 0; i < MinTable.Item2; i++)
                {
                    if (MinTableRows[i] != MaxTableRows[i])
                    {
                        //БЫЛО
                        DataRow newRow = difference.NewRow();
                        newRow.ItemArray = oldTable.Rows[i].ItemArray;
                        difference.Rows.Add(newRow);

                        //СТАЛО
                        DataRow newRow2 = difference.NewRow();
                        newRow2.ItemArray = newTable.Rows[i].ItemArray;
                        difference.Rows.Add(newRow2);
                    }
                }

                //учет добавленных/удаленных записей
                for (int i = MinTable.Item2; i < MaxTable.Item2; i++)
                {
                    if (!MinTable.Item1.Equals(oldTable))
                    {
                        DataRow row1 = difference.NewRow();
                        row1.ItemArray = MaxTable.Item1.Rows[i].ItemArray;
                        difference.Rows.Add(row1);

                        DataRow row2 = difference.NewRow();
                        difference.Rows.Add(row2);
                    }
                    else
                    {
                        DataRow newRow2 = difference.NewRow();
                        difference.Rows.Add(newRow2);

                        DataRow newRow = difference.NewRow();
                        newRow.ItemArray = MaxTable.Item1.Rows[i].ItemArray;
                        difference.Rows.Add(newRow);
                    }
                }

            difference.Columns.Add("Статус", typeof(string)).SetOrdinal(0);
            for (int i = 0; i < difference.Rows.Count; i++)
            {
                difference.Rows[i]["Статус"] = (i % 2 != 0) ? "Стало" : "Было";  
            }

            return difference;
        }
    }
}
