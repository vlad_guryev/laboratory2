﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.ComponentModel;

namespace WebParser
{
    class NetworkFileSaver
    {
        public delegate void DownLoadCompleted(object sender, AsyncCompletedEventArgs e);
        public delegate void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e);

        public void Download(string stringUrl, string targetFileName,
            DownloadProgressChanged onDownloadProgChanged, List<DownLoadCompleted> OnCompletedHandlers)
        {
            if (!Uri.TryCreate(stringUrl, UriKind.Absolute, out Uri url))
            {
                throw new Exception("provided url cannot be properly resolved");
            }
            else
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadProgressChanged +=
                        new DownloadProgressChangedEventHandler(onDownloadProgChanged);

                    foreach(var handler in OnCompletedHandlers)
                    {
                        webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(handler);
                    }

                    webClient.DownloadFileAsync(url, targetFileName);
                }
            }
        }


        public static bool TryInternetConnection(int timeOut = 3000, string url = "http://google.com/")
        {
            var task = CheckForInternetConnectionTask(timeOut, url);
            return task.Wait(timeOut) && task.Result;
        }

        private static Task<bool> CheckForInternetConnectionTask(int timeOut = 3000, string url = "http://google.com/")
        {
            return Task.Factory.StartNew(
                () =>
                {
                    try
                    {
                        var client = (HttpWebRequest)WebRequest.Create(url);
                        client.Method = "HEAD";
                        client.Timeout = timeOut;
                        using (var response = client.GetResponse())
                        using (response.GetResponseStream())
                        {
                            return true;
                        }
                    }
                    catch
                    {
                        return false;
                    }
                });
        }
    }
}
