﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.ComponentModel;
using System.Data;

namespace WebParser
{
    public partial class MainWindow : Window
    {
        private NetworkFileSaver networkFile;
        public string dbFileLocalPath { get; set; }
        public string dbURL { get; set; }
        List<DataTable> Pages { get; set; }
        public int PageSize { get; set; }
        DataTable CurrentDB { get; set; }
        DataTable ObsoleteDB { get; set; }
        List<NetworkFileSaver.DownLoadCompleted> OnDownloadCompletedHandlers;

        public MainWindow()
        {
            InitializeComponent();
            ContentRendered += new EventHandler(OnWindowStart);
            networkFile = new NetworkFileSaver();
            dbFileLocalPath = "dbThreats.xlsx";
            dbURL = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
            PageSize = 22;
            ThreatsGrid.CanUserSortColumns = false;
            ThreatsGrid.HeadersVisibility = DataGridHeadersVisibility.None;
            ThreatsGrid.IsReadOnly = true;
            OnDownloadCompletedHandlers = new List<NetworkFileSaver.DownLoadCompleted>
            {
                ProcessExistDb,
                DownloadHandler.DownloadCompletedHandler
            };
        }

        public void OnWindowStart(object sender, EventArgs args)
        {
            if (!File.Exists(dbFileLocalPath))
            {
                ProcessNotExistDb(this, new RoutedEventArgs());
            }
            else
            {
                ProcessExistDb(null, null);
            }
        }

        private void ProcessNotExistDb(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Файл локальной базы данных не существует на этом компьютере\nПроизвести загрузку файла БД из Интернета?",
                "Сохранение БД", MessageBoxButton.YesNo, MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
            {
                if (NetworkFileSaver.TryInternetConnection(3000, dbURL))
                {
                    try
                    {
                        networkFile.Download(dbURL, dbFileLocalPath, DownloadHandler.ProgressChangedHandler,
                             OnDownloadCompletedHandlers);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show("Ошибка при загрузке:" + exception.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Проверьте подключение к сети!");
                }
            }
        }

        private void ProcessExistDb(object sender, AsyncCompletedEventArgs e)
        {
            DataSet dbFile = ExcelFileReader.GetExcelDataSet(dbFileLocalPath);
            CurrentDB = dbFile.Tables["Sheet"];
            DataView view = new DataView(CurrentDB);
            DataTable customTableToView = view.ToTable(false, "Column0", "Column1");
            Pages = TableManager.Paginate(customTableToView, PageSize);
            PageValue = 1;
        }

        private void UpdateDbButton(object sender, RoutedEventArgs e)
        {
            ObsoleteDB = CurrentDB?.Copy();
            if (NetworkFileSaver.TryInternetConnection(3000, dbURL))
            {
                try
                {
                    networkFile.Download(dbURL, dbFileLocalPath, DownloadHandler.ProgressChangedHandler,
                        new List<NetworkFileSaver.DownLoadCompleted>() {
                        ProcessExistDb,
                        DownloadHandler.UpdateCompletedHandler,
                        ShowMergeInfo
                        });
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Ошибка при загрузке:" + exception.Message);
                }
            }
            else
            {
                MessageBox.Show("Проверьте подключение к сети!");
            }

        }
        private void ShowMergeInfo(object sender, AsyncCompletedEventArgs e)
        {
            if (ObsoleteDB != null)
            {
                EntryWindow EntryWindow = new EntryWindow();
                DataTable diff = TableManager.GetNewTableChanges(ObsoleteDB, CurrentDB);
                EntryWindow.BindDiffTableToDataGrid(diff);
                EntryWindow.Show(diff.Rows.Count / 2);
                ThreatsGrid.ItemsSource = Pages?.ElementAt(PageValue - 1).DefaultView;
            }
            else
            {
                MessageBox.Show("Файл БД создан!");
            }
        }

        private void SaveDbToFile(object sender, RoutedEventArgs e)
        {
            if (File.Exists(dbFileLocalPath))
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".xlsx";
                dlg.Filter = "Database document (.xlsx)|*.xlsx";

                if (dlg.ShowDialog() == true)
                {
                    string filename = dlg.FileName;
                    try
                    {
                        File.Copy(dbFileLocalPath, filename, true);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Возникла ошибка при записи" + exc.Message);
                    }
                    MessageBox.Show("Файл БД успешно сохранён!");
                }
            }
            else
            {
                MessageBox.Show("Файл БД не существует");
            }
        }

        private int currentPage = 0;
        public int PageValue
        {
            get
            {
                return currentPage;
            }
            set
            {
                currentPage = value;
                if ((currentPage <= Pages?.Count) && (currentPage > 0))
                    textPage.Text = currentPage.ToString();
            }
        }

        private void UpClick(object sender, RoutedEventArgs e)
        {
            if (PageValue < Pages?.Count)
            {
                PageValue++;
            }
        }

        private void DownClick(object sender, RoutedEventArgs e)
        {
            if (PageValue > 1)
            {
                PageValue--;
            }
        }

        private void TextPageChanged(object sender, TextChangedEventArgs e)
        {
            if(CurrentDB != null)
            {
                ThreatsGrid.ItemsSource = Pages?.ElementAt(PageValue - 1).DefaultView;
            }
        }

        private void SelectedRowBtnClicked(object sender, RoutedEventArgs e)
        {
            if(ThreatsGrid.SelectedIndex > 0)
            {
                int mapper(int selectedIndex) => selectedIndex + ((PageValue - 1) * PageSize);
                DataRow selectedRow = CurrentDB.Rows[mapper(ThreatsGrid.SelectedIndex)];
                var sb = new StringBuilder();
                foreach (var field in selectedRow.ItemArray)
                {
                    sb.Append(field.ToString() + " ");
                }
                
                EntryWindow EntryWindow = new EntryWindow();
                if(selectedRow != CurrentDB.Rows[1])
                {
                    EntryWindow.BindEntryToDataGrid(TableManager.BuildEntryTable(CurrentDB, selectedRow));
                    EntryWindow.Show();
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали ни одну позицию в списке!");
            }
        }
    }
}
