﻿using System.Windows;
using System.Windows.Controls;
using System.Data;

namespace WebParser
{
    /// <summary>
    /// Логика взаимодействия для EntryWindow.xaml
    /// </summary>
    public partial class EntryWindow : Window
    {
        public EntryWindow()
        {
            InitializeComponent();
            EntryGrid.IsReadOnly = true;
            EntryGrid.HeadersVisibility = DataGridHeadersVisibility.None;
            EntryGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        public void BindEntryToDataGrid(DataTable Entry)
        {
            Entry.Columns.RemoveAt(0);
            EntryGrid.ItemsSource = Entry.DefaultView;
        }
        public void BindDiffTableToDataGrid(DataTable Entry)
        {
            EntryGrid.ItemsSource = Entry.DefaultView;
        }

        public void Show(int i)
        {
            this.Show();
            MessageBox.Show("Обновлено записей: " + i.ToString());
        }
    }
}
