This is project that implements the _WPF application_ with the following functions:

*  Download db file from remote storage (excel file (*.xlsx, *.xls)
*  Update db file
*  Save file of db to local machine
*  Paginate over the entire list of entries from db